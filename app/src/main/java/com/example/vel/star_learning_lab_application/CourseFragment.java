package com.example.vel.star_learning_lab_application;

import android.app.Fragment;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class CourseFragment extends AppCompatActivity {
   private Fragment frg1,frg2;
   private Button list,home;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_fragment);

        list = (Button)findViewById(R.id.activity_list_fragment);
        home = (Button)findViewById(R.id.btnhome);


    }

    public void onButtonClick(View v){

        switch (v.getId()){
            case R.id.listFragment:
                startActivity(new Intent(CourseFragment.this,ListFragmentActivity.class));
                break;
            case R.id.listFragmentHome:
                startActivity(new Intent(CourseFragment.this,HomePage.class));
        }
    }
}
