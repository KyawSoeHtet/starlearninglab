package com.example.vel.star_learning_lab_application;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {
    public DatabaseHelper(Context context) {
        super(context, "Register.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("Create table user(email text primary key, password text)");


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists user");

    }
    //database insertion
    public boolean insert(String email,String password){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("email",email);
        contentValues.put("password",password);
        Long insert = db.insert("user",null,contentValues);
        if(insert==-1) return false;
        else return true;
    }
    public Boolean checkemail(String email){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("Select * from user where email=?", new String []{email});
        if (cursor.getCount()>0) return false;
        else return true;
    }
    //check email and password
    public Boolean emailpassword(String email,String password){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("Select * from user where email=? and password=?", new  String[]{email,password});
        if (cursor.getCount()>0)return true;
        else return false;
    }

    public boolean insert(String name,String address,String course){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name",name);
        contentValues.put("address",address);
        contentValues.put("course",course);
        Long insert = db.insert("user",null,contentValues);
        if(insert==-1) return false;
        else return true;
    }
    public Boolean checkname(String name){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("Select * from user where name=?", new String []{name});
        if (cursor.getCount()>0) return false;
        else return true;
    }
    //check name,address and course
    public Boolean nameaddresscourse(String name,String address,String course){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("Select * from user where name=? and address=? and password=?", new  String[]{name,address,course});
        if (cursor.getCount()>0)return true;
        else return false;
    }

}
