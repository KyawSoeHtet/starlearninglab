package com.example.vel.star_learning_lab_application;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    DatabaseHelper db;
   private Button create, login;
   private EditText email, pwd, conpwd;
   public static final String EMAIL_KEY = "EMAIL_KEY";
   public static final String PASSWORD_KEY = "PASSWORD_KEY";
   public static final String CONPASSWORD_KEY = "CONPASSWORD_KEY";
   private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = new DatabaseHelper(this);
        email = (EditText) findViewById(R.id.editemail);
        pwd = (EditText) findViewById(R.id.editpassword);
        conpwd = (EditText) findViewById(R.id.editcpassword);
        sharedPreferences = getSharedPreferences("MySharedPremain", Context.MODE_PRIVATE);
        create = (Button) findViewById(R.id.btncreate);
        login = (Button)findViewById(R.id.btnlogin);

        if(sharedPreferences.contains(EMAIL_KEY)){
            email.setText(sharedPreferences.getString(EMAIL_KEY,""));
        }
        if(sharedPreferences.contains(PASSWORD_KEY)){
            pwd.setText(sharedPreferences.getString(PASSWORD_KEY,""));
        }
        if(sharedPreferences.contains(CONPASSWORD_KEY)){
            conpwd.setText(sharedPreferences.getString(CONPASSWORD_KEY,""));
        }

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this,Login.class);
                startActivity(i);
            }
        });

        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String et1 = email.getText().toString();
                String et2 = pwd.getText().toString();
                String et3 =conpwd.getText().toString();

                if ( et1.equals("") || et2.equals("") || et3.equals("")){
                    Toast.makeText(getApplicationContext(),"Empty Field", Toast.LENGTH_SHORT).show();

                }else {
                    if (et2.equals(et3)){
                        Boolean checkemail = db.checkemail(et1);
                        if (checkemail==true){
                            Boolean insert = db.insert(et1,et2);
                            if (insert==true){
                                Toast.makeText(getApplicationContext()," Successfully Create Account",Toast.LENGTH_SHORT).show();
                            }
                            if (insert==true){
                                startActivity(new Intent(MainActivity.this,Login.class));
                            }
                        }
                        else {
                            Toast.makeText(getApplicationContext(),"Email Exists Already",Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        Toast.makeText(getApplicationContext(),"Password Not Match",Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });
    }
    public void creat(View view){
        SharedPreferences.Editor editor =sharedPreferences.edit();
        editor.putString(EMAIL_KEY,email.getText().toString());
        editor.putString(PASSWORD_KEY,pwd.getText().toString());
        editor.putString(CONPASSWORD_KEY,conpwd.getText().toString());
        editor.commit();
        Toast.makeText(view.getContext(),"data save",Toast.LENGTH_SHORT).show();

    }
}
