package com.example.vel.star_learning_lab_application;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Registration extends AppCompatActivity {
    DatabaseHelper db;
    private EditText name,address,course;
    private Button register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        db = new DatabaseHelper(this);
        name = (EditText)findViewById(R.id.editname);
        address = (EditText)findViewById(R.id.editaddress);
        course = (EditText)findViewById(R.id.editcourse);
        register = (Button)findViewById(R.id.btnregister);

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ed1 = name.getText().toString();
                String ed2 = address.getText().toString();
                String ed3 =course.getText().toString();


                if (ed1.equals("") || ed2.equals("") || ed3.equals("")){
                    Toast.makeText(getApplicationContext(),"Fields Is Empty",Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(getApplicationContext(),"Registration Successful",Toast.LENGTH_SHORT).show();
                    /*if (ed2!=ed3){
                        Boolean checkname =db.checkname(ed1);

                        if (checkname==true){
                            Boolean insert = db.insert(ed1,ed2,ed3);

                            if (insert==true){
                                Toast.makeText(getApplicationContext(),"Registration Successful",Toast.LENGTH_SHORT).show();
                            }
                        }
                        else {
                            Toast.makeText(getApplicationContext(),"Register Done Already",Toast.LENGTH_SHORT).show();
                        }
                    }*/

                }


            }
        });
    }
}
