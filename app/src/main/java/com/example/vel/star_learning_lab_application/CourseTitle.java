package com.example.vel.star_learning_lab_application;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class CourseTitle extends AppCompatActivity {
    private ListView listView;
    private Button home;
    private String[] coursename;
    private String[] coursedetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_title);

        home = (Button)findViewById(R.id.btnhome);


        coursename = getResources().getStringArray(R.array.coursename);
        coursedetail = getResources().getStringArray(R.array.coursedetails);


        listView = (ListView) findViewById(R.id.listViewSimple);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, coursedetail);
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Toast.makeText(getBaseContext()," Welcome To! " + coursename[position]+ " Class" , Toast.LENGTH_SHORT).show();
                        //Toast.makeText(getBaseContext(), ", Welcome To!"+candidateDetails[position] , Toast.LENGTH_SHORT).show();
                    }
                }
        );
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CourseTitle.this,HomePage.class));
            }
        });

    }
}
