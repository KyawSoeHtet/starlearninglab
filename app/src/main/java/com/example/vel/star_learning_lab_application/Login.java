package com.example.vel.star_learning_lab_application;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends AppCompatActivity {
    DatabaseHelper db;
    private EditText et1,et2;
    private Button logni;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        db  =new DatabaseHelper(this);
        et1 = (EditText)findViewById(R.id.editlname);
        et2 = (EditText)findViewById(R.id.editlpassword);
        logni = (Button)findViewById(R.id.btnllogin);

        logni.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = et1.getText().toString();
                String password = et2.getText().toString();
                Boolean checkemailpassword = db.emailpassword(email,password);

                if(checkemailpassword == true)
                    startActivity(new Intent(Login.this,HomePage.class));
                if (checkemailpassword==true)
                    Toast.makeText(getApplicationContext(),"Login Successful", Toast.LENGTH_SHORT).show();


                else
                    Toast.makeText(getApplicationContext(),"invalid Email Or Password",Toast.LENGTH_SHORT).show();

            }
        });
    }
}
