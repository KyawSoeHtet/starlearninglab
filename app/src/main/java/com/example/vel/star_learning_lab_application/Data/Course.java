package com.example.vel.star_learning_lab_application.Data;

public class Course {

    public static final String[]coursename = {
            "IELTS FOUR SKILL",
            "IELTS READING",
            "IELTS SPEAKING",
            "ACADEMIC WRITING",
            "FOUR SKILL BASIC",
            "FOUR SKILL PRE-INTERMEDIATE",
            "FOUR SKILL INTERMEDIATE",
            "GRAMMER PRE-INTERMEDIATE"
    };

    public static final String[] coursedetails = {
            "IELTS FOUR SKILL (WeekDay 9am to 1pm,   Duration Four Month,  Fees 150000Kyats,   Start Date June18)",
            "IELTS READING (WeekDay 7am to 9pm,   Duration Two and Half Month,  Fees 40000Kyats,   Start Date June25)",
            "IELTS SPEAKING (WeekDay 3pm to 5pm,   Duration Two Month,  Fees 40000Kyats,   Start Date June25)",
            "ACADEMIC WRITING (WeekDay 10am to 11.30am,   Duration Three Month,  Fees 50000Kyats,   Start Date June4)",
            "FOUR SKILL BASIC (WeekDay 8am to 10.30am,   Duration Two Month,  Fees 30000Kyats,   Start Date June4)",
            "FOUR SKILL PRE-INTERMEDIATE (Weekend 8am to 11am,   Duration Two Month,  Fees 35000Kyats,   Start Date June4)",
            "FOUR SKILL INTERMEDIATE (WeekDay 12pm to 2.30pm,   Duration Two Month,  Fees 40000Kyats,   Start Date June4)",
            "GRAMMER PRE-INTERMEDIATE(Weekend 7am to 10am,   Duration Three Month,  Fees 40000Kyats,   Start Date June4)"
    };

}
